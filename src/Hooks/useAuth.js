import axios from 'axios';
import React, { createContext, useContext, useState } from 'react';
import { useCookies } from 'react-cookie';
import { config } from '../config';

const authContext = createContext();

// Provider qui crée l'objet d'auth et gère les states
const useProvideAuth = () => {
  const [cookie, setCookie, removeCookie] = useCookies(['user']);
  const [user, setUser] = useState(cookie.user);
  const [error] = useState(null);
  const today = new Date();
  const tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

  // header de la requête http
  axios.defaults.headers.authorization = `Baerer ${cookie.user ? cookie.user.token : undefined}`;

  const signin = (email, password) => {
    return axios
      .post(`${config.BASE_API_URL}/auth/login`, {
        email,
        password,
      })
      .then(({ data, status }) => {
        if (status === 200) {
          setUser(data.user);
          setCookie('user', JSON.stringify(data.user), {
            expires: tomorrow,
          });
          localStorage.setItem('token', JSON.stringify(data.user.token));
        }
        return true;
      })
      .catch(e => {
        return e.response.data.error;
      });
  };

  const signup = (lastname, firstname, email, password) => {
    return axios
      .post(`${config.BASE_API_URL}/auth/signup`, {
        lastname,
        firstname,
        email,
        password,
      })
      .then(({ data, status }) => {
        if (status === 201) {
          setUser(data.user);
          setCookie('user', JSON.stringify(data.user), {
            expires: tomorrow,
          });
        }
        return true;
      })
      .catch(e => {
        return e.response.data.error;
      });
  };

  const signout = () => {
    setUser(null);
    removeCookie('user');
    localStorage.removeItem('token');
  };

  const getMyProfile = () => {
    return axios
      .get(`${config.BASE_API_URL}/auth/profile`)
      .then(response => {
        return response;
      })
      .catch(e => {
        return e.response.data.error;
      });
  };

  const editMyProfile = userData => {
    return axios
      .patch(`${config.BASE_API_URL}/user`, userData)
      .then(({ status, data }) => {
        if (status === 201) {
          setUser(data.user);
          setCookie('user', JSON.stringify(data.user), {
            expires: tomorrow,
          });
          localStorage.setItem('token', JSON.stringify(data.user.token));
          return true;
        }
      })
      .catch(e => {
        return e.response.data.error;
      });
  };

  const editMyPassword = data => {
    return axios
      .patch(`${config.BASE_API_URL}/user/edit-password`, data)
      .then(({ status }) => {
        if (status === 201) return true;
      })
      .catch(err => {
        return err.response.data.error;
      });
  };

  const forgotPassword = email => {
    return axios
      .post(`${config.BASE_API_URL}/auth/forgot-password`, { email })
      .then(({ status }) => {
        if (status === 200) return true;
      })
      .catch(err => {
        return err.response.data.error;
      });
  };

  const resetPassword = (password, id) => {
    return axios
      .post(`${config.BASE_API_URL}/auth/reset-password`, { password, id })
      .then(({ status }) => {
        if (status === 201) return true;
      })
      .catch(err => {
        return err.response.data.error;
      });
  };

  const joinNewsletter = email => {
    return axios
      .post(`${config.BASE_API_URL}/newsletter`, { email })
      .then(({ status }) => {
        if (status === 201) return true;
      })
      .catch(err => {
        return err.response.data.error;
      });
  };
  const removeAccount = () => {
    return axios
      .delete(`${config.BASE_API_URL}/user`)
      .then(({ status }) => {
        if (status === 200) {
          // suppression du stockage local
          localStorage.clear();
          // suppression du cookie
          removeCookie('user');
          // mise à jour des valeurs
          setUser(undefined);
        }
        return true;
      })
      .catch(e => {
        return e.response.data.error;
      });
  };

  return {
    user,
    signin,
    signup,
    signout,
    error,
    getMyProfile,
    editMyProfile,
    editMyPassword,
    forgotPassword,
    resetPassword,
    joinNewsletter,
    removeAccount,
  };
};

// Provider qui englobe l'app et rend l'objet d'auth
// accessible a tous les composants qui apppellent useAuth()
export function AuthProvider({ children }) {
  const auth = useProvideAuth();
  return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}

// Hook pour les composants enfants
// afin de disposer de l'objet d'auth et re-render quand il change
export const useAuth = () => {
  return useContext(authContext);
};

import React, { useState } from 'react';
import { Flex, FormLabel, FormControl, Input, Button, Alert, AlertIcon, AlertTitle, AlertDescription, Heading } from '@chakra-ui/core';
import { Link } from 'react-router-dom';
import MetaTags from 'react-meta-tags';
import { useAuth } from '../Hooks/useAuth';

const ReinitializationPassword = ({ match }) => {
  const { resetPassword } = useAuth();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [password, setPassword] = useState('');
  const [passwordBis, setPasswordBis] = useState('');
  // formulaire valide
  const [isValid, setIsValid] = useState(false);
  // succès de la requête pour changer son mdp
  const [success, setSuccess] = useState(false);
  // afficher le message d'erreur
  const [showError, setShowError] = useState({
    display: false,
    message: null,
  });

  // contraintes pour le mdp
  const [message, setMessage] = useState([
    {
      passwordLength: 'Le mot de passe doit contenir au moins 10 caractères',
      isValid: false,
    },
    {
      passwordMaj: 'Le mot de passe doit contenir une majuscule',
      isValid: false,
    },
    {
      passwordMin: 'Le mot de passe doit contenir une minuscule',
      isValid: false,
    },
    {
      passwordNum: 'Le mot de passe doit contenir un chiffre',
      isValid: false,
    },
  ]);

  return (
    <Flex flexDirection='column' alignItems='center' w='70%' mx='auto' py='80px' px={10}>
      <MetaTags>
        <title>Gusto Coffee : Réinitialisation de votre mot de passe</title>
        <meta name='description' content='Gusto Coffee : Une entreprise fort de café ! Réinitialisation de votre mot de passe' />
        <meta charSet='UTF-8' />
      </MetaTags>
      <Heading as='h2' mb='50px' color='grisFonce' size='md'>
        Entrez votre nouveau mot de passe
      </Heading>
      <form onSubmit={e => handleSubmit(e)} style={{ width: '50%' }}>
        <FormControl w='100%' mt='10px'>
          <FormLabel htmlFor='password'>Mot de passe</FormLabel>
          <Input
            onChange={e => {
              checkPassword(e.target.value);
            }}
            value={password}
            name='password'
            placeholder='************'
            type='password'
          />
          <p style={{ padding: 2, fontStyle: 'italic' }}>Le mot de passe doit contenir :</p>
          <p style={{ padding: 2, fontStyle: 'italic', color: message[0].isValid ? 'green' : 'red' }}>au moins 10 caractères</p>
          <p style={{ padding: 2, fontStyle: 'italic', color: message[1].isValid ? 'green' : 'red' }}>une majuscule</p>
          <p style={{ padding: 2, fontStyle: 'italic', color: message[2].isValid ? 'green' : 'red' }}>une minuscule</p>
          <p style={{ padding: 2, fontStyle: 'italic', color: message[3].isValid ? 'green' : 'red' }}>un chiffre.</p>
        </FormControl>
        <FormControl w='100%' mt='10px' isInvalid={!isValid}>
          <FormLabel htmlFor='password_bis'>Confirmez votre mot de passe</FormLabel>
          <Input
            onChange={e => checkPasswordBis(e.target.value)}
            value={passwordBis}
            name='password_bis'
            placeholder='************'
            type='password'
          />
        </FormControl>
        {requestSuccess()}
      </form>
    </Flex>
  );

  function requestSuccess() {
    // s'il y'a succès on affiche le message de succès avec redirection après 2.5s
    // si y'a erreur on affiche le message d'erreur
    // sinon on affiche le bouton pour envoyer la requête (par défaut)
    return success ? (
      <Alert status='success' mt='10px' variant='subtle' flexDirection='column' justifyContent='center' textAlign='center' height='200px'>
        <AlertIcon size='40px' mr={0} />
        <AlertTitle mt={4} mb={1} fontSize='lg'>
          Mise à jour du mot de passe réussie !
        </AlertTitle>
        <AlertDescription maxWidth='sm'>Vous pouvez fermer cette page</AlertDescription>
      </Alert>
    ) : showError.display ? (
      <Alert status='error' mt='10px' variant='subtle' flexDirection='column' justifyContent='center' textAlign='center' height='200px'>
        <AlertIcon size='40px' mr={0} />
        <AlertTitle mt={4} mb={1} fontSize='lg'>
          Erreur lors de la mise à jour du mot de passe
        </AlertTitle>
        <AlertDescription maxWidth='sm'>{showError.message()}</AlertDescription>
      </Alert>
    ) : (
      <Button mt={4} variantColor='teal' isLoading={isSubmitting} isDisabled={!isValid} type='submit'>
        Changer mon mot de passe
      </Button>
    );
  }
  function handleSubmit(e) {
    e.preventDefault();
    setIsSubmitting(true);
    resetPassword(password, match.params.id).then(data => {
      setIsSubmitting(false);
      if (data === true) {
        // affichage du message de succès + redirection
        setSuccess(true);
      } else {
        // affichage du message d'erreur avec un lien pour recommencer
        setShowError({
          display: true,
          message: () => (
            <>
              {data}
              <br />
              <Link style={{ textDecoration: 'underline' }} to='/mot-de-passe-oublie'>
                cliquez ici pour réessayer
              </Link>
            </>
          ),
        });
      }
    });
  }

  function checkPassword(pwd) {
    setPassword(pwd);
    const newMessage = message;
    // taille du mot de passe
    if (pwd.length >= 10) newMessage[0].isValid = true;
    else newMessage[0].isValid = false;
    // au moins un caractère en majuscule
    if (/^(?=.*[A-Z])/.test(pwd)) newMessage[1].isValid = true;
    else newMessage[1].isValid = false;
    // au moins un caractère en minuscule
    if (/^(?=.*[a-z])/.test(pwd)) newMessage[2].isValid = true;
    else newMessage[2].isValid = false;
    // au moins un chiffre
    if (/^(?=.*[0-9])/.test(pwd)) newMessage[3].isValid = true;
    else newMessage[3].isValid = false;

    setMessage(newMessage);
    checkFormulary(pwd, passwordBis);
  }
  function checkPasswordBis(pwdBis) {
    setPasswordBis(pwdBis);
    checkFormulary(password, pwdBis);
  }
  function checkFormulary(pwd, pwdBis) {
    // vérification que les 2 mdp correspondent
    // vérification que le mdp correspond à toutes les contraintes (undefined = tout est bon)
    if (pwd === pwdBis && message.find(val => !val.isValid) === undefined) setIsValid(true);
    else setIsValid(false);
  }
};

export default ReinitializationPassword;

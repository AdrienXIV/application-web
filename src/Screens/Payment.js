import {
  Box,
  Button,
  Heading,
  Stack,
  Text,
  useColorMode,
  useDisclosure,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalBody,
  ModalCloseButton,
} from '@chakra-ui/core';
import React, { useEffect, useState } from 'react';
import useCart from '../Hooks/useCart';
import { socket as io } from '../SocketIO';
import Stripe from '../Components/Stripe';

const PaymentModal = props => {
  const { isOpen, onClose, title, socket, basketId } = props;
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalCloseButton />
        <ModalBody>
          <Stripe title={title} basketId={basketId} socket={socket} onClose={onClose} />
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};
const Payment = ({ match }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { colorMode } = useColorMode();
  const [socket, setSocket] = useState(null);

  const { getBasket } = useCart();
  const [state, setState] = useState({});
  const [dataloaded, setDataloaded] = useState(false);

  useEffect(() => {
    setSocket(io);
    getBasket(match.params.id)
      .then(data => {
        setState(data.basket);
        setDataloaded(true);
      })
      .catch();
  }, [match.params.id]);

  if (!dataloaded) return <div />;
  return (
    <Box maxW='100%' m='0' py='80px' px='30px' backgroundColor={colorMode === 'light' ? 'white' : 'marron'}>
      <PaymentModal
        basketId={match.params.id}
        socket={socket}
        isOpen={isOpen}
        onOpen={onOpen}
        onClose={onClose}
        title={`${state.total_price.toFixed(2)}€`}
      />
      <Text fontSize='30px' fontWeight='bold' mb='50px' textAlign='center' id='recapitulate-order'>
        Récapitulatif de la commande
      </Text>
      <Stack spacing={8} mb='50px'>
        <Text fontSize='26px' fontWeight='bold' mb='30px'>
          Produits
        </Text>
        {state.articles && state.articles.length ? (
          state.articles.map((article, i) => (
            // eslint-disable-next-line react/no-array-index-key
            <Box p={5} shadow='md' borderWidth='1px' key={i}>
              <Heading mb='5px' fontSize='xl'>
                ({article.quantity}) {article.product_id.name}
              </Heading>
              <Text mt={4}>{`Sous-total : ${article.price}€`}</Text>
              <Text fontSize='12px'>{`Dont TVA : ${(article.price * 10) / 100}€`}</Text>
            </Box>
          ))
        ) : (
          <Text>Aucun produit.</Text>
        )}
      </Stack>
      <Stack spacing={8} my='50px'>
        <Text fontSize='26px' fontWeight='bold' mb='30px'>
          Réservations
        </Text>
        {state.reservations &&
          state.reservations.map(reservation => (
            <Box p={5} shadow='md' borderWidth='1px' key={reservation.reservation_id}>
              <Heading mb='5px' fontSize='xl'>
                Salon {reservation.reservation_id.location_id.name}
              </Heading>
              <Text mt={4}>
                Date : {String(reservation.reservation_id.day).padStart(2, '0')}/{String(reservation.reservation_id.month).padStart(2, '0')}/
                {reservation.reservation_id.year}
              </Text>
              <Text>Heure de début : {reservation.reservation_id.start}h</Text>
              <Text>Heure de fin : {reservation.reservation_id.end}h</Text>
              <Text mt={4}>Sous-total : {reservation.price}€</Text>
              <Text fontSize='12px'>{`Dont TVA : ${(reservation.price * 20) / 100}€`}</Text>
            </Box>
          ))}
      </Stack>
      <Button id='validate-order' onClick={onOpen} display='block' ml='auto' mt='50px' backgroundColor='marronClair' color='white'>
        {`Payer (${state.total_price.toFixed(2)}€)`}
      </Button>
    </Box>
  );
};

export default Payment;

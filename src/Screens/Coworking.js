import { Box, Button, Flex, Heading, Text, useColorMode } from '@chakra-ui/core';
import React from 'react';
import MetaTags from 'react-meta-tags';
import salleCommune from '../assets/images/salle-commune-big.jpg';
import CoworkingList from '../Components/CoworkingList';

const Coworking = () => {
  const { colorMode } = useColorMode();
  return (
    <Box backgroundColor={colorMode === 'light' ? 'white' : 'marron'}>
      <Box maxW='1400px' m='auto' py='80px'>
        <MetaTags>
          <title>Gusto Coffee : Salles de coworking</title>
          <meta
            name='description'
            content='Gusto Coffee : Une entreprise fort de café ! Réservez votre place ou votre salon privé chez Gusto Coffee! '
          />
          <meta charSet='UTF-8' />
        </MetaTags>
        <Heading as='h2' textAlign='center' mb='50px'>
          Nos salons privés
        </Heading>
        <CoworkingList />
        <Box maxW='1550px' m='auto' py='80px' px='50px'>
          <Heading textAlign='center' backgroundColor={colorMode === 'light' ? 'white' : 'marron'} mb='50px'>
            Notre salle principale
          </Heading>
          <Flex
            justifyContent='center'
            flexDirection='column'
            mt='100px'
            alignItems='center'
            background={`rgba(255, 255, 255) url(${salleCommune})`}
            minH='550px'
            backgroundPosition='center center'>
            <Text fontSize='40px' fontWeight='bold' color='blacl'>
              Reservez une place dans notre Open Space !
            </Text>
            <Box background='rgba(255, 255, 255, 0.6)' mt='80px' pt='50px' w='40%' p='20px'>
              <Text fontSize='18px' fontWeight='bold' mb='20px' mt='30px'>
                170 places disponibles
              </Text>
              <Text fontSize='18px' fontWeight='bold'>
                4€ / Heure
              </Text>
              <Text />
              <Button
                backgroundColor='marronClair'
                color='white'
                display='block'
                ml='auto'
                mt='auto'
                mb='10px'
                mr='20px'
                _hover={{ backgroundColor: 'marron' }}>
                Réserver
              </Button>
            </Box>
          </Flex>
        </Box>
      </Box>
    </Box>
  );
};

export default Coworking;

import React, { useState } from 'react';
import { Flex, FormLabel, FormControl, Input, Button, Alert, AlertIcon, AlertTitle, AlertDescription, Heading } from '@chakra-ui/core';
import { Redirect } from 'react-router-dom';
import { useAuth } from '../Hooks/useAuth';
import MetaTags from "react-meta-tags";

const ForgotPassword = () => {
  const { forgotPassword } = useAuth();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [email, setEmail] = useState('');
  // formulaire valide
  const [isValid, setIsValid] = useState(false);
  // succès de la requête pour changer son mdp
  const [success, setSuccess] = useState(false);
  // afficher le message d'erreur
  const [showError, setShowError] = useState({
    display: false,
    message: null,
  });

  const [redirect, setRedirect] = useState(false);

  if (redirect) return <Redirect to='/' />;
  return (
    <Flex flexDirection='column' alignItems='center' w='70%' mx='auto' py='80px' px={10}>
      <MetaTags>
        <title>Gusto Coffee : Mot de passe oublié</title>
        <meta name="description" content="Gusto Coffee : Une entreprise fort de café ! Perte de mot de passe ? Pas de soucis !" />
        <meta charSet="UTF-8"/>
      </MetaTags>
      <Heading as='h2' mb='50px' color='grisFonce' size='md'>
        Vous avez oublié votre mot de passe ? Entrez votre courriel pour recevoir un lien de réinitialisation
      </Heading>
      <form onSubmit={e => handleSubmit(e)} style={{ width: '50%' }}>
        <FormControl w='100%' mt='10px' isInvalid={!isValid}>
          <FormLabel htmlFor='email'>Votre courriel</FormLabel>
          <Input onChange={e => handleChange(e.target.value)} value={email} name='email' placeholder='exemple@gmail.com' type='email' />
        </FormControl>
        {requestSuccess()}
      </form>
    </Flex>
  );

  function requestSuccess() {
    // s'il y'a succès on affiche le message de succès avec redirection après 2.5s
    // si y'a erreur on affiche le message d'erreur
    // sinon on affiche le bouton pour envoyer la requête (par défaut)
    return success ? (
      <Alert status='success' mt='10px' variant='subtle' flexDirection='column' justifyContent='center' textAlign='center' height='200px'>
        <AlertIcon size='40px' mr={0} />
        <AlertTitle mt={4} mb={1} fontSize='lg'>
          Envoi réussi !
        </AlertTitle>
        <AlertDescription maxWidth='sm'>{`Un courriel vous a été envoyé à ${email}`}</AlertDescription>
        <AlertDescription maxWidth='sm'>Redirection automatique vers la page d'accueil</AlertDescription>
      </Alert>
    ) : showError.display ? (
      <Alert status='error' mt='10px' variant='subtle' flexDirection='column' justifyContent='center' textAlign='center' height='200px'>
        <AlertIcon size='40px' mr={0} />
        <AlertTitle mt={4} mb={1} fontSize='lg'>
          Erreur lors de l'envoi de la demande de réinitialisation
        </AlertTitle>
        <AlertDescription maxWidth='sm'>{showError.message}</AlertDescription>
      </Alert>
    ) : (
      <Button mt={4} variantColor='teal' isLoading={isSubmitting} isDisabled={!isValid} type='submit'>
        Confirmer
      </Button>
    );
  }

  function handleSubmit(e) {
    e.preventDefault();
    setIsSubmitting(true);
    forgotPassword(email).then(data => {
      setIsSubmitting(false);
      if (data === true) {
        // affichage du message de succès + redirection
        setSuccess(true);
        setTimeout(() => {
          setRedirect(true);
        }, 5000);
      } else {
        setShowError({
          display: true,
          message: `${data}, veuillez entrer un courriel enregistré sur Gusto Coffee`,
        });
      }
    });
  }

  function handleChange(value) {
    // vérification de la syntaxe du mail
    checkEmail(value);
    setEmail(value);
    // enlever le message d'erreur si on s'est trompé de mail
    setShowError(prev => ({ ...prev, display: false }));
  }
  /**
   * Vérification de la syntaxe du courriel
   * @param {String} e email
   */
  function checkEmail(e) {
    /\S+@\S+\.\S+/.test(e) ? setIsValid(true) : setIsValid(false);
  }
};

export default ForgotPassword;

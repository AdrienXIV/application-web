import { Box, Button, Flex, Image, Select, Text, useColorMode, useToast } from '@chakra-ui/core';
import React, { useEffect, useState } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { useCookies } from 'react-cookie';
import MetaTags from 'react-meta-tags';
import { useLocation } from 'react-router-dom';
import { useAuth } from '../Hooks/useAuth';
import useCart from '../Hooks/useCart';
import { getDayReservation } from '../utils/API';
import { hoursValue } from '../utils/reservationHours';
import { socket as io } from '../utils/socketIO';

const today = new Date();
const tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

const CoworkingDetail = () => {
  const { state } = useLocation();
  const { user } = useAuth();
  const { colorMode } = useColorMode();
  const toast = useToast();

  const { calculPoints, calculPrice } = useCart();
  const [reservationError, setReservationError] = useState(null);

  const [date, setDate] = useState(new Date());
  const [heureDebut, setHeureDebut] = useState('');
  const [heureFin, setHeureFin] = useState('');
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  const [reservedHours, setReservedHours] = useState({ start: [], end: [] });

  const handleChangeCalendar = newDate => {
    setDate(newDate);
  };

  const [cookie, setCookie] = useCookies(['shoppingBasket', 'reservation']);

  const [socket, setSocket] = useState(null);

  useEffect(() => {
    setSocket(io);
  }, []);

  useEffect(() => {
    (async () => {
      const dayToday = new Date().getDate();
      const monthToday = new Date().getMonth() + 1;
      const yearToday = new Date().getFullYear();
      const offset = 8;
      const temporaryStartArray = new Array(10).fill({ label: '', value: 0 });
      const temporaryEndArray = new Array(10).fill({ label: '', value: 0 });
      for (let i = 0; i <= 10; i++) {
        if (year >= yearToday && month >= monthToday && day >= dayToday) {
          if (i + offset >= new Date().getHours()) {
            temporaryStartArray[i] = { label: `${i + offset}h`, value: i + offset };
            // si ce sont els jours d'après le jour actuel ou les mois d'après
          } else if (day > dayToday || month > monthToday) {
            temporaryStartArray[i] = { label: `${i + offset}h`, value: i + offset };
          } else temporaryStartArray[i] = { label: `${i + offset}h - Réservation impossible`, value: 'reserve' };

          if (i + offset > new Date().getHours()) {
            temporaryEndArray[i] = { label: `${i + offset}h`, value: i + offset };
          } else if (day > dayToday || month > monthToday) {
            temporaryEndArray[i] = { label: `${i + offset}h`, value: i + offset };
          } else temporaryEndArray[i] = { label: `${i + offset}h - Réservation impossible`, value: 'reserve' };
        } else {
          temporaryStartArray[i] = { label: `${i + offset}h - Réservation impossible`, value: 'reserve' };
          temporaryEndArray[i] = { label: `${i + offset}h - Réservation impossible`, value: 'reserve' };
        }
      }

      const { data } = await getDayReservation(state.coworking._id, day, month, year);

      // On parcourt notre tableau de réservation
      data.forEach(reservation => {
        // On parcourt le tableau de modèle d'heures
        hoursValue.forEach((hour, i) => {
          // Si l'heure du modele est supérieure à l'heure courante
          // if (hour.value > new Date().getHours()) {
          // // Si l'heure du modele est plus petite que l'heure l'heure fin de la reservation
          if ((hour.value >= reservation.start && hour.value < reservation.end) || temporaryStartArray[i].value === 'reserve') {
            // On affiche que l'emplacement est deja reservé a cette heure
            if (temporaryStartArray[i].value !== 'reserve')
              temporaryStartArray[i] = { label: `${hour.value}h - Emplacement déja réservé`, value: 'reserve' };
          } else {
            // Sinon on affiche l'heure
            temporaryStartArray[i] = { label: `${hour.value}h`, value: hour.value };
          }

          // Si l'heure du modele est plus grande ou égale a l'heure de début de réservation
          // et plus petite ou égale a l'heure de fin de réservation
          if ((hour.value > reservation.start && hour.value <= reservation.end) || temporaryEndArray[i].value === 'reserve') {
            // On affiche que l'emplacement est deja reservé a cette heure
            if (temporaryEndArray[i].value !== 'reserve')
              temporaryEndArray[i] = { label: `${hour.value}h - Emplacement déja réservé`, value: 'reserve' };
          } else {
            // Sinon on affiche l'heure
            temporaryEndArray[i] = { label: `${hour.value}h`, value: hour.value };
          }
          // }
        });
      });
      // Sinon si on a pas de reservation sur cet espace

      setReservedHours({ start: temporaryStartArray, end: temporaryEndArray });
    })();
  }, [date, cookie.reservation]);

  // Méthode permettant d'ajouter une réservation au panier
  const makeReservation = () => {
    if (!user) {
      setReservationError(
        'Vous devez vous connecter afin de pouvoir réserver cet espace. Nous avons mis en place cette sécurité afin que personne ne puisse réserver le meme espace que vous le temps que vous validiez votre panier.',
      );
    } else if (heureDebut === '' || heureFin === '') {
      setReservationError('Veuillez séléctionner un horaire de début et de fin.');
    } else if (heureDebut === 'reserve' || heureFin === 'reserve') {
      setReservationError('Les horaires séléctionnés sont déja réservés.');
    } else {
      setReservationError(null);

      let error = false;
      const offset = 8; // 8h
      for (let i = 0; i < reservedHours.end.length; i++) {
        // si y'a déjà des emplacements réservées à l'intérieur d'un créneau utilisateur
        // exemple: 12h->16h déjà réservés et l'utilisateur souhaite réserver de 8h à 18h
        if (i + offset <= heureFin && i + offset > heureDebut) {
          if (reservedHours.end[i].value === 'reserve') {
            error = true;
          }
        }
      }
      if (error) {
        setReservationError('Veuillez séléctionner un créneau horaire valide.');
        return;
      }

      const data = {
        user_id: user.id,
        location_id: state.coworking._id,
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate(),
        start: +heureDebut,
        end: +heureFin,
        points: state.coworking.location_category_id.points,
        prix: state.coworking.location_category_id.price,
        name: state.coworking.name,
        location_category_id: state.coworking.location_category_id._id,
      };

      socket.emit(
        'new-hour-reservation',
        {
          user_id: data.user_id,
          location_id: data.location_id,
          year: data.year,
          month: data.month,
          day: data.day,
          start: data.start,
          end: data.end,
          price: calculPrice(data.prix, data.start, data.end),
          fidelity_points: calculPoints(data.points, data.start, data.end),
        },
        response => {
          if (response.code === 201) {
            const newReservations = cookie.reservation || [];
            newReservations.push({
              reservation_id: {
                _id: response.data._id,
                user_id: user ? user.userId : null,
                location_id: data.location_id,
                year: data.year,
                month: data.month,
                day: data.day,
                start: data.start,
                end: data.end,
                price: data.prix,
              },
              location_id: { name: data.name, location_category_id: data.location_category_id },
              fidelity_points: calculPoints(data.points, data.start, data.end),
              totalPrice: calculPrice(data.prix, data.start, data.end),
            });
            setCookie('reservation', newReservations, {
              expires: tomorrow,
            });
            toast(
              {
                title: 'Réservation ajoutée au panier.',
                status: 'success',
                duration: 2000,
                isClosable: true,
              },
              state.coworking,
            );
          } else {
            toast({
              title: response.error,
              status: 'error',
              duration: 3000,
              isClosable: true,
            });
          }
        },
      );
    }
  };

  return (
    <Box backgroundColor={colorMode === 'light' ? 'white' : 'marron'}>
      <Box maxWidth='1400px' mx='auto' px='20px' py='50px'>
        <MetaTags>
          <title>Gusto Coffee : Détails de la salle</title>
          <meta name='description' content='Gusto Coffee : Une entreprise fort de café ! Détails concernant nos salles de Coworking Gusto Coffee.' />
          <meta charet='UTF-8' />
        </MetaTags>
        <Text fontSize='26px' fontWeight='bold' mb='50px' maxWidth='1150px' mx='auto'>
          Salon {state.coworking.name}
        </Text>
        <Flex justifyContent='space-around' flexWrap='wrap' className='coworking-detail'>
          <Flex flexDirection='column' maxW='800px'>
            <Flex justifyContent='space-around' flexWrap='wrap'>
              <Image maxW='300px' src={require(`../assets/images/salon-${state.coworking.name}.jpg`)} mb='30px' />
              <Image maxW='300px' src={require(`../assets/images/salon-${state.coworking.name}.jpg`)} mb='30px' />
              <Image maxW='300px' src={require(`../assets/images/salon-${state.coworking.name}.jpg`)} mb='30px' />
              <Image maxW='300px' src={require(`../assets/images/salon-${state.coworking.name}.jpg`)} mb='30px' />
            </Flex>
          </Flex>
          <Flex flexDirection='column' alignItems='center'>
            <Calendar defaultActiveStartDate={date} value={date} onChange={handleChangeCalendar} />
            <Flex justifyContent='space-between' alignItems='center'>
              <Select
                placeholder='Heure de début'
                mt='50px'
                mr='10px'
                onChange={({ target }) => setHeureDebut(target.value)}
                fontSize='14px'
                w='180px'>
                {reservedHours.start &&
                  reservedHours.start.map(
                    (hour, index) =>
                      index !== reservedHours.start.length - 1 && (
                        <option value={hour.value} key={index.toString()} disabled={hour.value === 'reserve'}>
                          {hour.label}
                        </option>
                      ),
                  )}
              </Select>
              <Select placeholder='Heure de fin' mt='50px' onChange={({ target }) => setHeureFin(target.value)} fontSize='14px' w='180px'>
                {reservedHours.end &&
                  reservedHours.end
                    .filter((hour, index) => index !== 0)
                    .map((hour, index) => (
                      <option value={hour.value} key={index.toString()} style={{ color: 'black' }} disabled={hour.value === 'reserve'}>
                        {hour.label}
                      </option>
                    ))}
              </Select>
            </Flex>
          </Flex>
          {reservationError && (
            <Text fontSize='13px' style={{ color: 'red' }} mt='18px' textAlign='center' maxW='70%'>
              {reservationError}
            </Text>
          )}
        </Flex>
        <Flex justifyContent='space-between' px='80px' alignItems='end' mt='30px' flexWrap='wrap' id='coworking-details-infos'>
          <Flex flexDirection='column'>
            <Text>Nombre de places : {state.coworking.location_category_id.place_number}</Text>
            <Text>Prix : {state.coworking.location_category_id.price}€/heure</Text>
            <Text>Equipements : {state.coworking.location_category_id.stuff}</Text>
          </Flex>
          <Button
            display='block'
            ml='auto'
            mt='50px'
            mr='50px'
            backgroundColor='marronClair'
            _disabled={{ opacity: '0.8' }}
            color='white'
            onClick={makeReservation}>
            Réserver cet espace
          </Button>
        </Flex>
      </Box>
    </Box>
  );
};

export default CoworkingDetail;

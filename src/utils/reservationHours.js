export const hoursValue = [
  {
    label: '8h',
    value: 8,
  },
  {
    label: '9h',
    value: 9,
  },
  {
    label: '10h',
    value: 10,
  },
  {
    label: '11h',
    value: 11,
  },
  {
    label: '12h',
    value: 12,
  },
  {
    label: '13h',
    value: 13,
  },
  {
    label: '14h',
    value: 14,
  },
  {
    label: '15h',
    value: 15,
  },
  {
    label: '16h',
    value: 16,
  },
  {
    label: '17h',
    value: 17,
  },
  {
    label: '18h',
    value: 18,
  },
];

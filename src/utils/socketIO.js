import io from 'socket.io-client';
import { config } from '../config';

// options de la connexion web socket
const options = {
  query: { token: JSON.parse(localStorage.getItem('token')), room: 'NWS2020' },
  secure: true,
};

export const socket = io(config.BASE_API_URL, options);

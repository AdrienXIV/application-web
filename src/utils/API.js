import Axios from 'axios';
import { config } from '../config';

export function getDayReservation(id, day, month, year) {
  return Axios.get(`${config.BASE_API_URL}/reservation/${id}`, { params: { day, month, year } });
}

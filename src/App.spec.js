require('core-js/stable');
require('regenerator-runtime/runtime');
const puppeteer = require('puppeteer');
require('core-js/stable');

// global
let browser;
let page;
//
const HOST = 'http://localhost:3001';

const registerData = {
  email: 'test@test.com',
  password: 'Azertyuiop123!',
  firstname: 'Test',
  lastname: 'Test',
};
const stripeData = {
  cart: '4242424242424242',
  expire: '12/22',
  cvc: '123',
};

describe('Lancement page', () => {
  test('Accueil', async () => {
    browser = await puppeteer.launch({
      headless: false, // headless mode set to false so browser opens up with visual feedback
      slowMo: 50, // how slow actions should be
    });

    page = await browser.newPage();
    page.emulate({
      viewport: {
        width: document.documentElement.clientWidth,
        height: document.documentElement.clientHeight,
        deviceScaleFactor: window.devicePixelRatio,
      },
      userAgent: '',
    });

    await page.goto(HOST);
  }, 16000);
});
// buttonSignup;
describe("Page d'accueil", () => {
  test('Affichage titre de la page', async () => {
    const html = await page.$eval('h1', e => e.innerHTML);
    expect(html).toBe('Gusto Coffee Coworking');
  });

  test('Formulaire connexion/inscription', async () => {
    await page.click('#signin');
    await page.waitForSelector('#signupform');
    await page.click('#signupform');
    await page.waitForSelector('#signup-page');

    // remplir les inputs
    await page.click('input[name=email]');
    await page.type('input[name=email]', registerData.email);
    await page.click('input[name=password]');
    await page.type('input[name=password]', registerData.password);
    await page.click('input[name=firstname]');
    await page.type('input[name=firstname]', registerData.firstname);
    await page.click('input[name=lastname]');
    await page.type('input[name=lastname]', registerData.lastname);
    // appuyer sur le bouton s'inscrire
    await page.click('button[type=submit]');
    // inscription réussi donc redirection vers la page d'accueil
    const html = await page.$eval('h1', e => e.innerHTML);
    expect(html).toBe('Gusto Coffee Coworking');
  }, 15000);
});

describe('Produits', () => {
  test('Ajout du produit dans le panier', async () => {
    await page.click('#go-to-products');
    const html = await page.$eval('#product-title', e => e.innerHTML);
    expect(html).toBe('Nos produits');

    const button = await page.$eval('.product-button', e => e.innerHTML);
    expect(button).toBe('Ajouter au panier');
  }, 150000);

  test('Validation panier', async () => {
    await page.click('.product-button');
    await page.waitForSelector('#go-to-basket');
    await page.click('#go-to-basket');
    await page.waitForSelector('#my-basket');
    const html = await page.$eval('#my-basket', e => e.innerHTML);
    expect(html).toBe('Panier');
    await page.waitForSelector('#validate-basket');
    await page.click('#validate-basket');
  }, 15000);

  test('Récapitulatif de la commande', async () => {
    await page.waitForSelector('#recapitulate-order');
    const html = await page.$eval('#recapitulate-order', e => e.innerHTML);
    expect(html).toBe('Récapitulatif de la commande');
  }, 20000);

  // test('Paiement', async () => {
  //   await page.waitForSelector('#validate-order');
  //   await page.click('#validate-order');
  //   await page.waitForSelector('iframe');

  //   await page.click('input[name=cardnumber]');
  //   await page.type('input[name=cardnumber]', stripeData.cart);
  //   await page.click('input[name=exp-date]');
  //   await page.type('input[name=exp-date]', stripeData.expire);
  //   await page.click('input[name=cvc]');
  //   await page.type('input[name=cvc]', stripeData.cvc);
  //   await page.click('button[type=submit]');
  //   await page.waitForSelector('#notre-concept');
  //   const home = await page.$eval('#notre-concept', e => e.innerHTML);
  //   expect(home).toBe('Notre concept');
  // }, 15000);
});

describe('Profil', () => {
  test('Voir mon profil', async () => {
    await page.waitForSelector('#profile');
    await page.click('#profile');
    await page.waitForSelector('#remove-account');
    const remove = await page.$eval('#remove-account', e => e.innerHTML);
    expect(remove).toBe('Supprimer');
  }, 15000);

  test('Supprimer mon profil', async () => {
    await page.click('#remove-account');
    await page.waitForSelector('#confirm-remove-account');
    await page.click('#confirm-remove-account');
    await page.waitForSelector('h3');
    const html = await page.$eval('h3', e => e.innerHTML);
    expect(html).toBe('Connexion');
  }, 15000);

  // This function occurs after the result of each tests, it closes the browser
  afterAll(() => {
    browser.close();
  });
});

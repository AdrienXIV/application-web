import React, { useState } from 'react';
import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Box,
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  useColorMode,
} from '@chakra-ui/core';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../Hooks/useAuth';

const SignupForm = () => {
  const { signup } = useAuth();
  const { handleSubmit, errors, register } = useForm();
  const [isSubmitting, setIsSubmitting] = useState(false);

  const history = useHistory();

  // message si une erreur survint lors de la connexion
  const [formMessage, setFormMessage] = useState({
    show: false,
    message: '',
  });

  const validateEmail = value => {
    let e;
    const emailPattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!value) {
      e = 'Veuillez renseigner un email';
    } else if (!emailPattern.test(value)) {
      e = 'Veuillez entrer un email valide.';
    }
    return e || true;
  };

  const validatePassword = value => {
    let validationError;
    const passwordPattern = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{10,})/;
    if (!passwordPattern.test(value)) {
      validationError = 'Votre mot de passe doit contenur au minimum 10 caractères, dont un spécial, un chiffre et une majuscule';
    }
    return validationError || true;
  };

  const onSubmit = values => {
    // event.preventDefault();
    setIsSubmitting(true);
    // On appelle le service pour créer un nouvel utilisateur
    signup(values.lastname, values.firstname, values.email, values.password).then(data => {
      setIsSubmitting(false);
      if (data === true) {
        setFormMessage({
          show: false,
          message: '',
        });
        history.push('/');
      } else {
        setFormMessage({
          show: true,
          message: data,
        });
      }
    });
  };
  const { colorMode } = useColorMode();
  return (
    <Box w='66.66%' p={4} borderRadius={15} className='signup' backgroundColor={colorMode === 'light' ? 'white' : 'marron'} id='signup-page'>
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormControl w='100%' mt='10px' isInvalid={errors.firstname}>
          <FormLabel htmlFor='firstname'>Prénom</FormLabel>
          <Input name='firstname' placeholder='Jean-Paul' ref={register({ required: true })} />
          <FormErrorMessage>{errors.firstname && <span>Veuillez renseigner un prénom</span>}</FormErrorMessage>
        </FormControl>
        <FormControl w='100%' isInvalid={errors.lastname}>
          <FormLabel htmlFor='lastname'>Nom</FormLabel>
          <Input name='lastname' placeholder='Belmondo' ref={register({ required: true })} />
          <FormErrorMessage>{errors.lastname && <span>Veuillez renseigner un nom</span>}</FormErrorMessage>
        </FormControl>
        <FormControl w='100%' mt='10px' isInvalid={errors.email}>
          <FormLabel htmlFor='email'>Email</FormLabel>
          <Input name='email' placeholder='jean-paul@gmail.com' ref={register({ validate: validateEmail })} />
          <FormErrorMessage>{errors.email && errors.email.message}</FormErrorMessage>
        </FormControl>
        <FormControl w='100%' mt='10px' isInvalid={errors.password}>
          <FormLabel htmlFor='password'>Mot de passe (min. 10 caractères, dont un chiffre, une majuscule et un caractère spécial)</FormLabel>
          <Input name='password' placeholder='************' ref={register({ validate: validatePassword })} type='password' />
          {/* TODO : Demander un mot de passe sécurisé et confirmation */}
          <FormErrorMessage>{errors.password && errors.password.message}</FormErrorMessage>
        </FormControl>
        {requestSuccess()}
        <Button mt={4} variantColor='teal' isLoading={isSubmitting} type='submit'>
          S'inscrire
        </Button>
      </form>
    </Box>
  );

  function requestSuccess() {
    return formMessage.show ? (
      <Alert status='error' mt='10px' variant='subtle' flexDirection='column' justifyContent='center' textAlign='center' height='200px'>
        <AlertIcon size='40px' mr={0} />
        <AlertTitle mt={4} mb={1} fontSize='lg'>
          Erreur lors de l'inscription
        </AlertTitle>
        <AlertDescription maxWidth='sm'>{formMessage.message}</AlertDescription>
      </Alert>
    ) : null;
  }
};

export default SignupForm;

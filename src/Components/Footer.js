import { Box, Button, Flex, Icon, Input, Link, List, ListItem, Stack, Text } from '@chakra-ui/core';
import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { useAuth } from '../Hooks/useAuth';
import BackToTop from './backToTopButton';

const Footer = () => {
  const { joinNewsletter } = useAuth();
  const [email, setEmail] = useState('');
  // email valide pour la newsletter
  const [isValid, setIsValid] = useState(false);
  // succès de la requête d'inscription à la newsletter
  // 0 : état initial
  // 1 : succès
  // 2 : échec
  const [success, setSuccess] = useState(0);
  // message d'erreur de la requête pour s'inscrire à la newsletter
  const [error, setError] = useState('');

  return (
    <Box as='footer' bg='#414141' color='white' position='relative'>
      <Flex alignItems='center' justifyContent='space-around' flexWrap='wrap' px='20px' pt='40px' maxWidth='1550px' margin='auto'>
        <Flex flexDirection='column' flex='0 1 350px' mb='50px'>
          <List>
            <ListItem py='8px'>
              <Link as={RouterLink} to='/cgu-cgv'>
                CGU/CGV
              </Link>
            </ListItem>
            <ListItem py='8px'>
              <Link as={RouterLink} to='/mentions-legales'>
                Mentions Légales
              </Link>
            </ListItem>
            <ListItem py='8px'>
              <Link as={RouterLink} to='/cookies'>
                Politiques de confidentialité
              </Link>
            </ListItem>
          </List>
        </Flex>
        <Flex flexDirection='column' flex='0 1 350px' mb='50px'>
          <Box>
            <Link as={RouterLink} to='/contact'>
              Contact
            </Link>
            <Text pb='8px' pt='12px'>
              01.35.24.55.90
            </Text>
            <Text py='8px'>contact@gusto-coffee.fr</Text>
          </Box>
        </Flex>
        <Flex flex='0 1 350px' flexDirection='column' mb='50px'>
          <Stack mb='50px'>
            <Stack isInline align='center'>
              <Input
                isDisabled={success === 1}
                onChange={({ target }) => {
                  handleChange(target.value);
                }}
                style={{ borderTopRightRadius: 0, borderBottomRightRadius: 0 }}
                placeholder='Inscrivez-vous à la Newsletter'
                size='md'
                w='80%'
                color='black'
              />
              {requestSuccess()}
            </Stack>
            {showError()}
          </Stack>

          <Flex justifyContent='space-between' width='80%' alignItems='center' p='10px'>
            <Link href='https://www.facebook.com/gustoCoffeeParis' isExternal>
              <Icon name='facebook' size='35px' />
            </Link>
            <Link href='https://twitter.com/GustoCoffee2' isExternal>
              <Icon name='twitter' size='35px' />
            </Link>
            <Link href='https://www.instagram.com/gustocoffeeparis/' isExternal>
              <Icon name='instagram' size='35px' />
            </Link>
          </Flex>
        </Flex>
      </Flex>
      <Flex alignItems='center' justifyContent='space-around' flexWrap='wrap' px='20px' pb='30px' maxWidth='1350px' margin='auto'>
        <Button backgroundColor='black' color='white' px='30px' py='25px' display='flex' alignItems='center' justifyContent='space-between' mb='30px'>
          <Icon name='appStore' size='20px' color='white' />
          <Link to='#' ml='10px' textAlign='initial'>
            Download on the
            <br />
            App Store
          </Link>
        </Button>
        <Button backgroundColor='black' color='white' px='30px' py='25px' display='flex' alignItems='center' justifyContent='space-between' mb='30px'>
          <Icon name='playStore' size='20px' color='white' />
          <Link to='https://drive.google.com/file/d/1GLjYKvauH4faFnIrLDJ0cdnFY0HeEb_R/view?usp=sharing' ml='10px' textAlign='initial'>
            Download on the
            <br />
            Play Store
          </Link>
        </Button>
      </Flex>
      <BackToTop />
    </Box>
  );

  // affichage du bouton s'inscrire par défaut
  // si l'inscription est réussie, on affiche un icon de succès
  // sinon on affiche un icon d'erreur
  function requestSuccess() {
    return success === 1 ? (
      <Icon name='check' size='32px' color='green.500' />
    ) : success === 2 ? (
      <Icon name='not-allowed' size='32px' color='red.500' />
    ) : (
      <Button
        isDisabled={!isValid}
        onClick={() => singupToNewsletter()}
        style={{ borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
        variantColor='green'>
        S'inscrire
      </Button>
    );
  }

  function showError() {
    return success === 2 ? <Text>{error}</Text> : null;
  }

  function handleChange(value) {
    setSuccess(0);
    setEmail(value);
    checkEmail(value);
  }

  function checkEmail(e) {
    /\S+@\S+\.\S+/.test(e) ? setIsValid(true) : setIsValid(false);
  }
  function singupToNewsletter() {
    if (isValid) {
      joinNewsletter(email).then(data => {
        if (data === true) setSuccess(1);
        else {
          setError(data);
          setSuccess(2);
        }
      });
    }
  }
};

export default Footer;

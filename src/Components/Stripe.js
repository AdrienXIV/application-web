import React, { useEffect, useState } from 'react';
import { CardElement, Elements, useElements, useStripe } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import axios from 'axios';
import { useCookies } from 'react-cookie';
import { CircularProgress, useToast } from '@chakra-ui/core';
import { Redirect } from 'react-router-dom';
import { config } from '../config';

const CheckoutForm = ({ socket, basketId, onClose, title }) => {
  const [redirect, setRedirect] = useState(false);
  const [paymentWaiting, setPaymentWaiting] = useState(false);
  const [, , removeCookie] = useCookies(['shoppingBasket', 'reservation']);
  const stripe = useStripe();
  const elements = useElements();
  const toast = useToast();

  useEffect(() => {
    return () => {
      setPaymentWaiting(false);
      onClose(); // fermeture modal
    };
  }, []);

  const handleSubmit = async e => {
    e.preventDefault();
    setPaymentWaiting(true);
    if (!stripe || !elements) {
      setPaymentWaiting(false);
      toast({
        title: 'Erreur lors du paiement',
        description: 'Veuillez entrer vos informations bancaires',
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
      // Stripe.js has not loaded yet. Make sure to disable
      // form submission until Stripe.js has loaded.
      return;
    }

    // Get a reference to a mounted CardElement. Elements knows how
    // to find your CardElement because there can only ever be one of
    // each type of element.
    const cardElement = elements.getElement(CardElement);

    // Use your card Element with other Stripe.js APIs
    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: 'card',
      card: cardElement,
    });

    if (error) {
      setPaymentWaiting(false);
      toast({
        title: 'Erreur lors du paiement',
        description: error.message,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
      return;
    }
    try {
      const { id } = paymentMethod;

      const { data } = await axios.post(`${config.BASE_API_URL}/order/payment`, {
        _id: basketId,
        currency: 'euro',
        payment_method: 'bank card',
        id_stripe: id,
      });
      setPaymentWaiting(false);
      // si la personne est connecté
      socket.emit('new-order', { _id: data._id });

      // supprimer le stockge local
      localStorage.removeItem('basket');
      removeCookie('shoppingBasket');
      removeCookie('reservation');
      localStorage.setItem('confirmBasket', JSON.stringify(0));
      toast({
        title: data.message,
        description: "redirection vers la page d'accueil",
        status: 'success',
        duration: 2000,
        isClosable: true,
      });

      setTimeout(() => {
        setRedirect(true);
      }, 1500);
    } catch (err) {
      setPaymentWaiting(false);
      toast({
        title: 'Erreur',
        description: err.response.data.error,
        status: 'error',
        duration: 5000,
        isClosable: true,
      });
    }
  };

  const CARD_ELEMENT_OPTIONS = {
    iconStyle: 'solid',
    hidePostalCode: true,
    style: {
      base: {
        iconColor: 'rgb(240, 57, 122)',
        color: 'rgb(240, 57, 122)',
        fontSize: '16px',
        fontFamily: '"Open Sans", sans-serif',
        fontSmoothing: 'antialiased',
        '::placeholder': {
          color: '#CFD7DF',
        },
      },
      invalid: {
        color: '#e5424d',
        ':focus': {
          color: '#303238',
        },
      },
    },
  };
  if (redirect) return <Redirect to='/' />;

  return (
    <div>
      <div className='product-info'>
        <h3 id='payment-title' className='product-title'>
          Commande Gusto Coffee
        </h3>
        <h4 className='product-price'>{title}</h4>
      </div>
      <form onSubmit={handleSubmit}>
        <CardElement options={CARD_ELEMENT_OPTIONS} />
        {!paymentWaiting && (
          <button type='submit' disabled={!stripe} className='btn-pay'>
            Valider
          </button>
        )}
      </form>
      <div style={{ textAlign: 'center' }}>{paymentWaiting && <CircularProgress isIndeterminate color='green.300' />}</div>
    </div>
  );
};
// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.
const stripePromise = loadStripe('pk_test_51IACXNLf1MKoIUjX7DHMaABjRE5BULK9BuZas3jPu3BWBq8qLWvP9wYGZ6xpXhWix9jzUTA89Gnd6Mv5pozvl1vr00ZH6Atqzg');

const Stripe = ({ socket, basketId, onClose, title }) => {
  return (
    <Elements stripe={stripePromise}>
      <CheckoutForm title={title} socket={socket} basketId={basketId} onClose={onClose} />
    </Elements>
  );
};

export default Stripe;
